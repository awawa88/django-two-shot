from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.form import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required
def home(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipts = form.save(False)
            receipts.purchaser = request.user
            receipts.save()

            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def categories_list(request):
    expense = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense": expense}
    return render(request, "receipts/categories.html", context)

@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"accountList": account}
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()

            return redirect("categories_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            acc = form.save(False)
            acc.owner = request.user
            acc.save()

            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_accounts.html", context)
